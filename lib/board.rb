class Board
  attr_accessor :grid

  def initialize(grid = Board.default_grid)
    @grid = grid
  end

  def [](idx)
    @grid[idx[0]][idx[1]]
  end

  def []=(idx, value)
    @grid[idx] = value
  end

  def self.default_grid
    return Board.new(Array.new(10) { Array.new(10) }).grid
  end

  def count
    counter = 0
    @grid.each do |row|
      row.each do |cell|
        if cell == :s
          counter += 1
        end
      end
    end
    return counter
  end

  def empty?(pos = "none")
    if pos == "none"
      @grid.each do |row|
        row.each do |cell|
          if cell == :s || cell == :x
            return false
          end
        end
      end
      return true
    else
      x, y = pos
      if @grid[x][y] == nil
        return true
      else
        return false
      end
    end
  end

  def full?
    @grid.each do |row|
      row.each do |cell|
        if cell == nil
          return false
        end
      end
    end
    return true
  end

  def place_random_ship
    if full?
      raise_error
    else
      x_range = []
      y_range = []
      (0...@grid.length).each { |n| x_range.push(n) }
      (0...@grid[0].length).each { |n| y_range.push(n) }
      x = x_range.sample
      y = y_range.sample
      while @grid[x][y] != nil
        x = x_range.sample
        y = y_range.sample
      end
      @grid[x][y] = :s
    end
  end

  def won?
    @grid.each do |row|
      row.each do |cell|
        if cell == :s
          return false
        end
      end
    end
    return true
  end

end
